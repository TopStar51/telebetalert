<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOddsEvolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odds_evolution', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('alert_id')->unsigned();
            $table->string('odds1');
            $table->string('odds2');
            $table->string('odds3')->nullable();
            $table->timestamps();
            $table->index('alert_id');
        });

        Schema::table('odds_evolution', function(Blueprint $table) {
            $table->foreign('alert_id')->references('id')->on('alerts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odds_evolution');
    }
}
