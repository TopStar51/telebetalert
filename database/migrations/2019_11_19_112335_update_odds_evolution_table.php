<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOddsEvolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('odds_evolution', function (Blueprint $table) {
            $table->string('odds4')->nullable();
            $table->string('odds5')->nullable();
            $table->string('odds6')->nullable();
            $table->string('odds7')->nullable();
            $table->string('odds8')->nullable();
            $table->string('odds9')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odds_evolution');
    }
}
