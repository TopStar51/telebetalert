<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeagueEvolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_evolution', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('league_id')->unsigned();
            $table->string('match_name');
            $table->string('odd1');
            $table->string('odd2');
            $table->string('odd3');
            $table->timestamps();
            $table->index('league_id');

        });

        Schema::table('league_evolution', function(Blueprint $table) {
            $table->foreign('league_id')->references('id')->on('leagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_evolution');
    }
}
