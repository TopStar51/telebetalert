<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarketGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $market_groups = [
            ['group_name' => 'United Kingdom'],
            ['group_name' => 'UEFA Competitions'],
            ['group_name' => 'Italy'],
            ['group_name' => 'Spain'],
            ['group_name' => 'Germany'],
            ['group_name' => 'France'],
            ['group_name' => 'Internationals'],
            ['group_name' => 'Europe'],
            ['group_name' => 'Sweden'],
            ['group_name' => 'Denmark'],
            ['group_name' => 'Norway'],
            ['group_name' => 'Rep of Ireland'],
            ['group_name' => 'Australia'],
            ['group_name' => 'Rest of the World'],
            ['group_name' => 'The Americas']
        ];
        
        foreach($market_groups as $market_group) {
            DB::table('market_group')->insert($market_group);
        }
    }
}
