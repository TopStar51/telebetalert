<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['register' => false]); // Do not need the registration, only need login

Route::get('/', 'CreateAlertController@index');
Route::get('/create-alert', 'CreateAlertController@index')->name('create.alert');
Route::post('/create-alert', 'CreateAlertController@save')->name('save.alert');

Route::get('/alert-list','ViewAlertListController@index')->name('list.alert');
Route::post('/alert-list', 'ViewAlertListController@destroy')->name('alerts.delete');
Route::post('/getAlertList', 'ViewAlertListController@getAlertList')->name('getAlertList');

Route::get('/league-list', 'ViewLeagueListController@index')->name('list.league');
Route::post('/league-list', 'ViewLeagueListController@destroy')->name('leagues.delete');
Route::post('/getLeagueList', 'ViewLeagueListController@getLeagueList')->name('getLeagueList');
Route::post('/league-list/add', 'ViewLeagueListController@create')->name('leagues.add');
Route::post('/league-list/changeStatus', 'ViewLeagueListController@changeStatus')->name('leagues.changeStatus');
Route::post('/league-list/playall', 'ViewLeagueListController@playAll')->name('leagues.playall');
Route::post('/league-list/pauseall', 'ViewLeagueListController@pauseAll')->name('leagues.pauseall');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile', 'ProfileController@update')->name('update.profile');

Route::get('/user-list','UserListController@index')->name('list.user');
Route::post('/user-list/add', 'UserListController@create')->name('users.add');
Route::post('/user-list', 'UserListController@destroy')->name('users.delete');
Route::post('/getUserList', 'UserListController@getUserList')->name('getUserList');
