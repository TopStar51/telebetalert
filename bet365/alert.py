from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from datetime import datetime
from random import randint
from bs4 import BeautifulSoup
from mysql.connector import Error
from mysql.connector.connection import MySQLConnection
from mysql.connector import pooling
import time
import mysql.connector
import requests
import json

with open('config.json') as config_file:
    config_data = json.load(config_file)

bot_token = config_data['alert_bot_token']
bot_chat_id = config_data['alert_bot_chatid']

BET1 = "Double Chance"
BET2 = "Asian Handicap"
BET3 = "Textual Search"
BET4 = "Half Time/Full Time"

user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36"

def telegram_bot_sendtext(bot_message):
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chat_id + '&parse_mode=HTML&text=' + bot_message
    response = requests.get(send_text)
    return response.json()

def alert_expire(con_obj, cursor, cur_time, alert_id):
    cursor.execute("UPDATE alerts SET status = {}, expired_at = '{}' WHERE id = {}".format(2, cur_time, alert_id))
    con_obj.commit()
    cursor.execute("DELETE FROM odds_evolution WHERE alert_id={}".format(alert_id))
    con_obj.commit()

if __name__ == '__main__':

    # All the ugly Try-Except blocks are to decrease the runtime of the script. Without them, the script hits errors
    # with trying to access elements which haven't loaded on the page yet. The Try-Except loop basically just keeps
    # trying to access the elements until they've loaded. The other solution I tried was to sleep() for a set time
    # before trying to access the elements. However, it seriously reduced the runtime so I'm sticking with the ugliness.

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument('--user-agent={}'.format(user_agent))
    # options.add_argument("--headless")
    # options.add_argument("start-maximized")
    # options.add_argument("--disable-gpu")
    # options.add_argument("--disable-extensions")
    # options.add_argument("--no-sandbox")
    # proxy = get_proxy()
    # options.add_argument('--proxy-server={}:{}'.format(proxy[0], proxy[1]))
    driver = webdriver.Chrome(options=options)

    driver.get('https://www.bet365.com/')
    time.sleep(randint(5, 10))
    try:
        driver.find_element_by_link_text("English").click()
        time.sleep(randint(5, 10))
    except:
        pass
    # site language section
    lang_dropdown_elem = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'hm-LanguageDropDownSelections')))
    # lang_dropdown_elem = driver.find_element_by_class_name('hm-LanguageDropDownSelections')
    sel_lang = lang_dropdown_elem.find_element_by_css_selector('span.hm-DropDownSelections_Highlight').text.strip()
    if sel_lang != "English":
        dropdown_btn = lang_dropdown_elem.find_element_by_class_name('hm-DropDownSelections_Button')
        driver.execute_script("arguments[0].click();", dropdown_btn)
        time.sleep(2)
        english_btn = lang_dropdown_elem.find_element_by_class_name('hm-DropDownSelections_Container').find_element_by_link_text('English')
        driver.execute_script("arguments[0].click();", english_btn)
        time.sleep(randint(5, 8))

    while True:
        # if int(time.strftime("%d"))%2: # Odd day is not running
        #    print('break')
        #    break
        cur_hour = time.strftime('%H')
        if int(cur_hour) == 23:
            break
        
        try:
            connection_pool = mysql.connector.pooling.MySQLConnectionPool(
                pool_name = "pynative_alert_pool",
                pool_size = 5,
                pool_reset_session = True,
                host = config_data['database_host'],
                database = config_data['database_name'],
                user = config_data['database_user'],
                password = config_data['database_pass'],
                charset = config_data['charset'])

            # Get connection object from a pool    
            connection_object = connection_pool.get_connection()

            if connection_object.is_connected():
                cursor = connection_object.cursor()
                cursor.execute("SELECT id, type, url, start_time, end_time, match_name, status FROM alerts WHERE status != 2")
                results = cursor.fetchall()
                for result in results:
                    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    if result[3] <= now and result[4] >= now:
                        if driver.current_url == result[2]:
                            driver.refresh()
                        else:
                            driver.get(result[2])
                        time.sleep(randint(6, 10))
                        soup = BeautifulSoup(driver.page_source, 'html.parser')
                        time.sleep(randint(3, 5))
                        try:
                            market_grid = soup.find('div', {'class': 'gl-MarketGrid'})
                        except:
                            alert_expire(connection_object, cursor, now, result[0])
                            continue
                        if result[1] == BET1 or result[1] == BET2 or result[1] == BET4:
                            try:
                                marketgroup_buttons = market_grid.find_all('div', {'class': 'gl-MarketGroupButton_Text'})
                            except:
                                continue
                            if len(marketgroup_buttons) == 0:
                                alert_expire(connection_object, cursor, now, result[0])
                                continue
                            for marketgroup_button in marketgroup_buttons:
                                if result[1] in marketgroup_button.text:
                                    marketgroup_wrapper = marketgroup_button.parent.findNext('div', {'class': 'gl-MarketGroup_Wrapper'})
                                    if result[1] == BET1:
                                        participant_name = marketgroup_wrapper.find_all('span', {'class': 'gl-Participant_Name'})
                                        odd_value = marketgroup_wrapper.find_all('span', {'class': 'gl-Participant_Odds'})

                                        team1 = participant_name[0].text[:-7]
                                        odds1x = odd_value[0].text
                                        team2 = participant_name[1].text[8:]
                                        oddsx2 = odd_value[1].text
                                        odds12 = odd_value[2].text

                                        cursor.execute('SELECT odds1, odds2, odds3 FROM `odds_evolution` WHERE `alert_id` = {}'.format(result[0]))
                                        evolution = cursor.fetchone()

                                        args1 = {'team1': team1, 'team2': team2}
                                        args2 = {'curtime': datetime.now().strftime("%m/%d %H:%M")}
                                        args3 = {'odds1x': odds1x, 'oddsx2': oddsx2, 'odds12': odds12}
                                        message = '<b>{team1}</b> vs <b>{team2}</b>\n'.format(**args1) + '      {curtime} :\n'.format(**args2) + '              1x      /       x2      /       12\n' + '               {odds1x}    /   {oddsx2}    /   {odds12}'.format(**args3)

                                        if evolution is None:
                                            cursor.execute("INSERT INTO odds_evolution (alert_id, odds1, odds2, odds3, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s)", (result[0], odds1x, oddsx2, odds12, now, now))
                                            connection_object.commit()
                                            print(message)
                                            telegram_bot_sendtext(message)
                                        else :
                                            if evolution[0] != odds1x or evolution[1] != oddsx2 or evolution[2] != odds12:
                                                cursor.execute("UPDATE odds_evolution SET odds1 = {}, odds2 = {}, odds3 = {}, updated_at = '{}' WHERE alert_id = {}".format(odds1x, oddsx2, odds12, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), result[0]))
                                                connection_object.commit()
                                                print(message)
                                                telegram_bot_sendtext(message)
                                    elif result[1] == BET2:
                                        teams = marketgroup_wrapper.find_all('div', {'class': 'gl-MarketColumnHeader'})
                                        handicap = marketgroup_wrapper.find_all('span', {'class': 'gl-ParticipantCentered_Name'})
                                        odds = marketgroup_wrapper.find_all('span', {'class': 'gl-ParticipantCentered_Odds'})

                                        team1 = teams[0].text
                                        team2 = teams[1].text
                                        handicap1 = handicap[0].text
                                        handicap2 = handicap[1].text
                                        odds1 = odds[0].text
                                        odds2 = odds[1].text

                                        cursor.execute('SELECT odds1, odds2 FROM `odds_evolution` WHERE `alert_id` = {}'.format(result[0]))
                                        evolution = cursor.fetchone()
                                        args1 = {'team1': team1, 'team2': team2}
                                        args2 = {'curtime': datetime.now().strftime("%m/%d %H:%M")}
                                        args3 = {'handicap1': handicap1, 'odds1': odds1, 'handicap2': handicap2, 'odds2': odds2}
                                        message = '<b>{team1}</b> vs <b>{team2}</b>\n'.format(**args1) + '      {curtime} :\n'.format(**args2) + '              {handicap1}  {odds1}     /     {handicap2}  {odds2}'.format(**args3)
                                        if evolution is None:
                                            cursor.execute("INSERT INTO odds_evolution (alert_id, odds1, odds2, created_at, updated_at) VALUES (%s, %s, %s, %s, %s)", (result[0], odds1, odds2, now, now))
                                            connection_object.commit()
                                            print(message)
                                            telegram_bot_sendtext(message)
                                        else :
                                            if evolution[0] != odds1 or evolution[1] != odds2:
                                                cursor.execute("UPDATE odds_evolution SET odds1 = '{}', odds2 = '{}', updated_at = '{}' WHERE alert_id = {}".format(odds1, odds2, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), result[0]))
                                                connection_object.commit()
                                                print(message)
                                                telegram_bot_sendtext(message)
                                    else:
                                        participant_names = marketgroup_wrapper.find_all('span', {'class': 'gl-ParticipantBorderless_Name'})
                                        odd_values = marketgroup_wrapper.find_all('span', {'class': 'gl-ParticipantBorderless_Odds'})
                                        cursor.execute('SELECT odds1, odds2, odds3, odds4, odds5, odds6, odds7, odds8, odds9 FROM `odds_evolution` WHERE `alert_id` ={}'.format(result[0]))
                                        evolution = cursor.fetchone()
                                        args1 = {'pt1': participant_names[0].text, 'odds1': odd_values[0].text, 'pt2': participant_names[1].text, 'odds2': odd_values[1].text, 'pt3': participant_names[2].text, 'odds3': odd_values[2].text}
                                        args2 = {'pt4': participant_names[3].text, 'odds4': odd_values[3].text, 'pt5': participant_names[4].text, 'odds5': odd_values[4].text, 'pt6': participant_names[5].text, 'odds6': odd_values[5].text}
                                        args3 = {'pt7': participant_names[6].text, 'odds7': odd_values[6].text, 'pt8': participant_names[7].text, 'odds8': odd_values[7].text, 'pt9': participant_names[8].text, 'odds9': odd_values[8].text}
                                        message = '<b>{pt1}</b> {odds1}    <b>{pt2}</b> {odds2}    <b>{pt3}</b> {odds3}\n'.format(**args1) + '\n' + '<b>{pt4}</b> {odds4}    <b>{pt5}</b> {odds5}    <b>{pt6}</b> {odds6}\n'.format(**args2) + '\n' + '<b>{pt7}</b> {odds7}    <b>{pt8}</b> {odds8}    <b>{pt9}</b> {odds9}'.format(**args3)
                                        if evolution is None:
                                            cursor.execute("INSERT INTO odds_evolution (alert_id, odds1, odds2, odds3, odds4, odds5, odds6, odds7, odds8, odds9, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)",
                                            (result[0], odd_values[0].text, odd_values[1].text, odd_values[2].text, odd_values[3].text, odd_values[4].text, odd_values[5].text, odd_values[6].text, odd_values[7].text, odd_values[8].text, now, now))
                                            connection_object.commit()
                                            print(message)
                                            telegram_bot_sendtext(message)
                                        else :
                                            if evolution[0] != odd_values[0].text or evolution[1] != odd_values[1].text or evolution[2] != odd_values[2].text or evolution[3] != odd_values[3].text or evolution[4] != odd_values[4].text or evolution[5] != odd_values[5].text or evolution[6] != odd_values[6].text or evolution[7] != odd_values[7].text or evolution[8] != odd_values[8].text:
                                                cursor.execute("UPDATE odds_evolution SET odds1 = '{}', odds2 = '{}', odds3 = '{}', odds4 = '{}', odds5 = '{}', odds6 = '{}', odds7 = '{}', odds8 = '{}', odds9 = '{}', updated_at = '{}' WHERE alert_id = {}".format(odd_values[0].text, odd_values[1].text, odd_values[2].text, odd_values[3].text, odd_values[4].text, odd_values[5].text, odd_values[6].text, odd_values[7].text, odd_values[8].text, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), result[0]))
                                                connection_object.commit()
                                                print(message)
                                                telegram_bot_sendtext(message)
                                    cursor.execute("UPDATE alerts SET status = {} WHERE id = {}".format(1, result[0]))
                                    connection_object.commit()
                                    break
                                else:
                                    continue
                        else:
                            try:
                                marketgroup_wrapper = market_grid.find('div', {'class': 'sl-MarketCouponFixtureLabelBase gll-Market_General gll-Market_HasLabels'})
                                match_name_divs = marketgroup_wrapper.find_all('div', {'class': 'sl-CouponParticipantWithBookCloses_Name'})

                                for match_name_div in match_name_divs:
                                    if (result[5].lower() in match_name_div.text.lower()) and result[6] == 0:
                                        cursor.execute("UPDATE alerts SET status = {} WHERE id = {}".format(1, result[0]))
                                        connection_object.commit()
                                        message = "Published: {}".format(match_name_div.text)
                                        print(message)
                                        telegram_bot_sendtext(message)
                                    else:
                                        continue
                            except:
                                alert_expire(connection_object, cursor, now, result[0])
                                continue
                    elif result[4] < now:
                        alert_expire(connection_object, cursor, now, result[0])
                        continue
                    else:
                        continue
        except Error as e :
            print ("Error while connecting to MySQL using Connection pool ", e)
        finally:
            #closing database connection.
            if(connection_object.is_connected()):
                cursor.close()
                connection_object.close()
            time.sleep(randint(6, 9))
    time.sleep(5)
    driver.close()
    driver.quit()
