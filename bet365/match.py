from selenium import webdriver
from datetime import datetime
from random import randint
import time
import mysql.connector
import requests
import json

with open('config.json') as config_file:
    config_data = json.load(config_file)

chromedriver_path = config_data['driver_path']
bot_token = config_data['league_bot_token']
bot_chat_id = config_data['league_bot_chatid']

SOCCER_URL = 'https://www.bet365.com/#/AS/B1/'

def telegram_bot_sendtext(bot_message):
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chat_id + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    return response.json()

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
# options.add_argument("--headless")
options.add_argument("start-maximized")
# options.add_argument("--disable-gpu")
# options.add_argument("--no-sandbox")
# proxy = get_proxy()
# options.add_argument('--proxy-server={}:{}'.format(proxy[0], proxy[1]))
driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)

if __name__ == '__main__':

    # All the ugly Try-Except blocks are to decrease the runtime of the script. Without them, the script hits errors
    # with trying to access elements which haven't loaded on the page yet. The Try-Except loop basically just keeps
    # trying to access the elements until they've loaded. The other solution I tried was to sleep() for a set time
    # before trying to access the elements. However, it seriously reduced the runtime so I'm sticking with the ugliness.

    driver.get('https://www.bet365.com/')
    time.sleep(10)
    driver.find_element_by_link_text("English").click()
    time.sleep(10)

    while True:
        db = mysql.connector.connect(
            host = config_data['database_host'],
            user = config_data['database_user'],
            passwd = config_data['database_pass'],
            database = config_data['database_name'],
            charset = config_data['charset'],
        )
        # print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        if driver.current_url == SOCCER_URL:
            driver.refresh()
        else:
            driver.get(SOCCER_URL)
        time.sleep(randint(7, 10))
        cursor = db.cursor()
        cursor.execute("SELECT leagues.id, name, group_name, status FROM leagues \
            LEFT JOIN market_group ON market_group.id = leagues.group_id \
            WHERE status = 1 ORDER BY group_id")
        results = cursor.fetchall()

        for result in results:
            try:
                market_container_div = driver.find_element_by_class_name('sm-SplashContainer')
                full_time_group = market_container_div.find_elements_by_class_name('sm-MarketGroup')[0]
                market_groups = full_time_group.find_elements_by_class_name('sm-Market')
                for market_group in market_groups:
                    group_name = market_group.find_element_by_class_name('sm-Market_GroupName').text
                    if result[2] == group_name:
                        try:
                            market_header_div = market_group.find_element_by_class_name('sm-Market_HeaderClosed')
                            market_header_div.click()
                            time.sleep(randint(5, 7))
                        except:
                            pass
                        leagues = market_group.find_elements_by_class_name('sm-CouponLink_Label')
                        break
                for league in leagues:
                    is_forward = False
                    if league.text == result[1]:
                        league.click()
                        time.sleep(randint(5, 7))
                        url = driver.current_url
                        driver.get(url)
                        time.sleep(randint(5, 7))
                        is_forward = True
                        break
                if is_forward == True:
                    try:
                        group_container = driver.find_element_by_class_name("gll-MarketGroupContainer")
                        fixture_label = group_container.find_element_by_class_name("sl-MarketCouponFixtureLabelBase")
                        odd_container = group_container.find_elements_by_class_name("sl-MarketCouponValuesExplicit33")
                        participant_divs = fixture_label.find_elements_by_class_name("sl-CouponParticipantWithBookCloses_NameContainer")
                        odd1_divs = odd_container[0].find_elements_by_class_name("gll-ParticipantOddsOnly_Odds")
                        odd2_divs = odd_container[1].find_elements_by_class_name("gll-ParticipantOddsOnly_Odds")
                        odd3_divs = odd_container[2].find_elements_by_class_name("gll-ParticipantOddsOnly_Odds")
                        match_names = []

                        for participant_div in participant_divs:
                            try:
                                live_score_div = participant_div.find_element_by_class_name("pi-ScoreVariantCentred")
                                participant1 = participant_div.find_elements_by_class_name("sl-CouponParticipantWithBookCloses_Name")[0].text
                                participant2 = participant_div.find_elements_by_class_name("sl-CouponParticipantWithBookCloses_Name")[1].text
                                match_names.append("{} v {}".format(participant1, participant2))
                                continue
                            except:
                                match_names.append(participant_div.find_element_by_class_name("sl-CouponParticipantWithBookCloses_Name").text)
                        i = 0
                        message = "{}\r\n".format(result[1])
                        is_update = False
                        while i<len(match_names):
                            cursor.execute("SELECT odd1, odd2, odd3, id FROM league_evolution WHERE match_name='{}'".format(match_names[i]))
                            odds = cursor.fetchone()
                            if odds is None:
                                is_update = True
                                message += "    {}  \r\n        1       /       x       /       2       \r\n        {}    /     {}    /    {}   \r\n".format(match_names[i], odd1_divs[i].text, odd2_divs[i].text, odd3_divs[i].text)
                                cursor.execute("INSERT INTO league_evolution (league_id, match_name, odd1, odd2, odd3) VALUES (%s, %s, %s, %s, %s)", (result[0], match_names[i], odd1_divs[i].text, odd2_divs[i].text, odd3_divs[i].text))
                                db.commit()
                            else:
                                if odds[0] != odd1_divs[i].text or odds[1] != odd2_divs[i].text or odds[2] != odd3_divs[i].text:
                                    is_update = True
                                    message += "    {}  \r\n        1       /       x       /       2       \r\n        {}   /   {}   /   {}  \r\n".format(match_names[i], odd1_divs[i].text, odd2_divs[i].text, odd3_divs[i].text)
                                    cursor.execute("UPDATE league_evolution SET odd1 = '{}', odd2 = '{}', odd3 = '{}' WHERE id = {}".format(odd1_divs[i].text, odd2_divs[i].text, odd3_divs[i].text, odds[3]))
                                    db.commit()
                            i += 1
                        if is_update == True:
                            print(message)
                            telegram_bot_sendtext(message)
                    except:
                        pass
                    driver.back()
                    time.sleep(randint(5, 8))
                else:
                    continue
            except:
                continue
