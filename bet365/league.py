from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from datetime import datetime
from random import randint
from mysql.connector import Error
from mysql.connector.connection import MySQLConnection
from mysql.connector import pooling
import time
import mysql.connector
import requests
import json

SOCCER_URL = 'https://www.bet365.com/#/AS/B1/'

with open('config.json') as config_file:
    config_data = json.load(config_file)

# chromedriver_path = config_data['driver_path']
bot_token = config_data['league_bot_token']
bot_chat_id = config_data['league_bot_chatid']

user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"

def telegram_bot_sendtext(bot_message):
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chat_id + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    return response.json()

if __name__ == '__main__':

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument('--user-agent={}'.format(user_agent))
    # options.add_argument("--lang=en-GB")
    # options.add_argument("--headless")
    # options.add_argument("--disable-gpu")
    # options.add_argument("--disable-extensions")
    # options.add_argument("--no-sandbox")
    # proxy = get_proxy()
    # options.add_argument('--proxy-server={}:{}'.format(proxy[0], proxy[1]))
    driver = webdriver.Chrome(options=options)

    driver.get('https://www.bet365.com/')
    time.sleep(randint(5, 10))
    try:
        driver.find_element_by_link_text("English").click()
        time.sleep(randint(5, 10))
    except:
        pass
    # site language section
    # lang_dropdown_elem = driver.find_element_by_class_name('hm-LanguageDropDownSelections')
    lang_dropdown_elem = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'hm-LanguageDropDownSelections')))
    sel_lang = lang_dropdown_elem.find_element_by_css_selector('span.hm-DropDownSelections_Highlight').text.strip()
    if sel_lang != "English":
        dropdown_btn = lang_dropdown_elem.find_element_by_class_name('hm-DropDownSelections_Button')
        driver.execute_script("arguments[0].click();", dropdown_btn)
        time.sleep(3)
        english_btn = lang_dropdown_elem.find_element_by_class_name('hm-DropDownSelections_Container').find_element_by_link_text('English')
        driver.execute_script("arguments[0].click();", english_btn)
        time.sleep(randint(5, 8))

    while True:
        cur_hour = time.strftime('%H')
        if int(cur_hour) == 23:
            break
        try:
            connection_pool = mysql.connector.pooling.MySQLConnectionPool(
                pool_name = "pynative_league_pool",
                pool_size = 5,
                pool_reset_session = True,
                host = config_data['database_host'],
                database = config_data['database_name'],
                user = config_data['database_user'],
                password = config_data['database_pass'],
                charset = config_data['charset'])

            # Get connection object from a pool    
            connection_object = connection_pool.get_connection()

            if connection_object.is_connected():
                # print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                cursor = connection_object.cursor()
                if driver.current_url == SOCCER_URL:
                    driver.refresh()
                else:
                    driver.get(SOCCER_URL)
                time.sleep(randint(6, 10))
                try:
                    market_container_div = driver.find_element_by_class_name('slm-SplashContainer')
                    full_time_group = market_container_div.find_elements_by_class_name('slm-MarketGroup')[0]
                    market_groups = full_time_group.find_elements_by_class_name('slm-Market')
                    for market_group in market_groups:
                        try:
                            market_header_div = market_group.find_element_by_class_name('slm-Market_HeaderClosed')
                            market_header_div.click()
                            time.sleep(randint(5, 7))
                        except:
                            pass
                        group_name = market_group.find_element_by_class_name('slm-Market_GroupName').text
                        cursor.execute('SELECT `leagues`.id, `leagues`.name, `leagues`.status FROM `leagues` LEFT JOIN `market_group` ON `leagues`.group_id = `market_group`.id WHERE `market_group`.group_name = "{}" AND `leagues`.status != {}'.format(group_name, 2))
                        results = cursor.fetchall()
                        leagues = market_group.find_elements_by_class_name('slm-CouponLink_Label')
                        for result in results:
                            published = False
                            for league in leagues:
                                if league.text == result[1]:
                                    if result[2] == 1:
                                        published = True
                                        break
                                    cursor.execute("UPDATE leagues SET status = %s WHERE id = %s", (1, result[0]))
                                    connection_object.commit()
                                    published = True
                                    message = "Published: {}".format(result[1])
                                    print(message)
                                    telegram_bot_sendtext(message)
                                    break
                            if published == False:
                                cursor.execute("UPDATE leagues SET status = %s WHERE id = %s", (0, result[0]))
                                connection_object.commit()
                                cursor.execute("DELETE FROM league_evolution WHERE league_id={}".format(result[0]))
                                connection_object.commit()
                                if result[2] == 1:
                                    message = "Expired: {}".format(result[1])
                                    print(message)
                                    telegram_bot_sendtext(message)
                except:
                    pass
        except Error as e :
            print ("Error while connecting to MySQL using Connection pool ", e)
        finally:
            #closing database connection.
            if(connection_object.is_connected()):
                cursor.close()
                connection_object.close()
            time.sleep(randint(6, 9))
    time.sleep(5)
    driver.close()
    driver.quit()
