<?php

namespace App\Http\Controllers;

use App\League;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewLeagueListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $groups = DB::table('market_group')->get();
        return view('league-list', ['groups' => $groups]);
    }

    public function create(Request $request)
    {
        $league_name = $request->league_name;
        $group_id = $request->group_id;
        DB::table('leagues')->insert([
            ['name' => $league_name, 'group_id' => $group_id]
        ]);

        return response()->json(['status' => true]);
    }

    public function destroy(Request $request)
    {
        $league_id = $request->league_id;

        DB::table('league_evolution')->where('league_id', '=', $league_id)->delete();
        DB::table('leagues')->where('id', '=', $league_id)->delete();

        return response()->json(['status' => true]);
    }

    public function changeStatus(Request $request)
    {
        $league_id = $request->league_id;
        if($request->status == '2') {
            DB::table('leagues')->where('id', '=', $league_id)->update(['status' => '0']);
        } else {
            DB::table('leagues')->where('id', '=', $league_id)->update(['status' => '2']);
        }
        return response()->json(['status' => true]);
    }

    public function playAll(Request $request){
        DB::table('leagues')->where('status', '=', '2')->update(['status' => '0']);
        return response()->json(['status' => true]);
    }

    public function pauseAll(Request $request){
        DB::table('leagues')->where('status', '!=', '2')->update(['status' => '2']);
        return response()->json(['status' => true]);
    }

    public function getLeagueList(Request $request)
    {
        $columns = array(
            0 => 'name',
            1 => 'group_name',
            2 => 'status',
            3 => 'id'
        );

        $totalData = DB::table('leagues')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');

        if(empty($request->input('search.value')))
        {
            if($request->league_status == "") {
                $leagues = DB::table('leagues')
                ->leftJoin('market_group', 'leagues.group_id', '=', 'market_group.id')
                ->select('name', 'group_name', 'status', 'leagues.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy('group_id', 'ASC')
                ->get();
            } else {
                $leagues = DB::table('leagues')
                ->leftJoin('market_group', 'leagues.group_id', '=', 'market_group.id')
                ->select('name', 'group_name', 'status', 'leagues.id')
                ->where('status', $request->league_status)
                ->offset($start)
                ->limit($limit)
                ->orderBy('group_id', 'ASC')
                ->get();

                $totalFiltered = DB::table('leagues')->where('status', $request->league_status)
                        ->count();
            }
        }
        else {
            $search = $request->input('search.value');
            if($request->league_status == "") {
                $leagues =  DB::table('leagues')
                ->leftJoin('market_group', 'leagues.group_id', '=', 'market_group.id')
                ->select('name', 'group_name', 'status', 'leagues.id')
                ->where('name','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy('group_id', 'ASC')
                ->get();

                $totalFiltered = DB::table('leagues')->where('name','LIKE',"%{$search}%")->count();
            } else {
                $leagues =  DB::table('leagues')
                ->leftJoin('market_group', 'leagues.group_id', '=', 'market_group.id')
                ->select('name', 'group_name', 'status', 'leagues.id')
                ->where('name','LIKE',"%{$search}%")
                ->where('status', $request->league_status)
                ->offset($start)
                ->limit($limit)
                ->orderBy('group_id', 'ASC')
                ->get();

                $totalFiltered = DB::table('leagues')->where('name','LIKE',"%{$search}%")->where('status', $request->league_status)->count();
            }

        }

        $data = array();
        if(!empty($leagues))
        {
            foreach ($leagues as $league)
            {
                $status = '';
                switch($league->status) {
                    case 1:
                        $status = '<span class="btn btn-pill btn-success">Published</span>';
                        break;
                    case 2:
                        $status = '<span class="btn btn-pill btn-danger">Pause</span>';
                        break;
                    default:
                        $status = '<span class="btn btn-pill btn-default">Not Published</span>';
                        break;
                }

                $action = '';
                if($league->status == 2) {
                    $action = "&emsp;<button onclick='changeStatus(".$league->id.','.$league->status.")' class='btn btn-app btn-secondary mr-2 mt-1 mb-1'><i class='fa fa-play'></i></button>
                    <button onclick='removeLeague(".$league->id.")' class='btn btn-app btn-danger mr-2 mt-1 mb-1'><i class='fa fa-trash'></i></button>";
                } else {
                    $action = "&emsp;<button onclick='changeStatus(".$league->id.','.$league->status.")' class='btn btn-app btn-info mr-2 mt-1 mb-1'><i class='fa fa-pause'></i></button>
                    <button onclick='removeLeague(".$league->id.")' class='btn btn-app btn-danger mr-2 mt-1 mb-1'><i class='fa fa-trash'></i></button>";
                }

                $nestedData['name'] = $league->name;
                $nestedData['group_name'] = $league->group_name;
                $nestedData['status'] = $status;
                $nestedData['actions'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
            );

        echo json_encode($json_data);
    }
}
