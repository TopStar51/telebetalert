<?php

namespace App\Http\Controllers;

use App\Alert;
use Illuminate\Http\Request;


class CreateAlertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('create-alert');
    }

    public function save(Request $request)
    {
        //Validate
        $request->validate([
            'type' => 'required',
            'match_name' => 'required',
            'url' => 'required|url',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_date' => 'required',
            'end_time' => 'required'
        ]);
        
        $start_time = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time));
        $end_time = date('Y-m-d H:i:s', strtotime($request->end_date.' '.$request->end_time));
 
        Alert::create([
            'type' => $request->type,
            'match_name' => $request->match_name,
            'url' => $request->url,
            'start_time' => $start_time,
            'end_time' => $end_time
        ]);

        return redirect('/alert-list');
    }
}
