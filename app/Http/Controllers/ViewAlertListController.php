<?php

namespace App\Http\Controllers;

use App\Alert;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ViewAlertListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('view-list');
    }

    public function destroy(Request $request)
    {
        $alertId = $request->alertId;

        DB::table('odds_evolution')->where('alert_id', '=', $alertId)->delete();
        DB::table('alerts')->where('id', '=', $alertId)->delete();
        
        return response()->json(['status' => true]);
    }

    public function getAlertList(Request $request)
    {
        $columns = array( 
            0 =>'type', 
            1 =>'match_name',
            2=> 'start_time',
            3=> 'end_time',
            4=> 'status',
            5=> 'id'
        );

        $totalData = Alert::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
        $alerts = Alert::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
        $search = $request->input('search.value'); 

        $alerts =  Alert::where('type','LIKE',"%{$search}%")
                    ->orWhere('match_name', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        $totalFiltered = Alert::where('type','LIKE',"%{$search}%")
                    ->orWhere('match_name', 'LIKE',"%{$search}%")
                    ->count();
        }

        $data = array();
        if(!empty($alerts))
        {
            foreach ($alerts as $alert)
            {
                $status = '';
                switch($alert->status) {
                    case 1:
                        $status = '<span class="btn btn-pill btn-success">Published</span>';
                        break;
                    case 2:
                        $status = '<span class="btn btn-pill btn-danger">Expired</span>';
                        break;
                    default:
                        $status = '<span class="btn btn-pill btn-default">Not Published</span>';
                        break;
                }

                $nestedData['type'] = $alert->type;
                $nestedData['match_name'] = $alert->match_name;
                $nestedData['start_time'] = $alert->start_time;
                $nestedData['end_time'] = $alert->end_time;
                $nestedData['status'] = $status;
                $nestedData['actions'] = "&emsp;<button type='button' onclick='removeAlert(".$alert->id.")' title='Delete' class='btn btn-icon btn-primary btn-danger'><i class='fe fe-trash'></i></button>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 
    }
}
