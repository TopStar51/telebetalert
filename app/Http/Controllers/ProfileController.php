<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile');
    }

    public function update(Request $request)
    {
        if(!empty($request->current_password)) {
            // Validate
            $request->validate([
                'email' => 'email|required',
                'current_password' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required'
            ]);

            $user = DB::table('users')->where('id', Auth::user()->id)->first();
            if(Hash::check($request->current_password, $user->password)) {
                $new_password = $request->password;
                DB::table('users')->where('id', Auth::user()->id)->update([
                    'email' => $request->email,
                    'password' => Hash::make($new_password),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                return redirect()->back()->with('message', 'Updated successfully! Please try to login again');
            } else {
                $messages = 'The current password is not correct!';
                return redirect('/profile')->withErrors($messages);
            }
        } else {
            $request->validate([
                'email' => 'email|required'
            ]);
            DB::table('users')->where('id', Auth::user()->id)->update([
                'email' => $request->email,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            return redirect()->back()->with('message', 'Updated successfully!');
        }
    }
}
