<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Hash;
use Carbon\Carbon;

class UserListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user-list');
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = Hash::make($request->password);
        $created_at = Carbon::now()->toDateTimeString();

        DB::table('users')->insert([
            ['name' => $name, 'email' => $email, 'password' => $password, 'created_at' => $created_at]
        ]);

        return response()->json(['status' => true]);
    }

    public function destroy(Request $request)
    {
        $user_id = $request->user_id;

        DB::table('users')->where('id', '=', $user_id)->delete();

        return response()->json(['status' => true]);
    }

    public function getUserList(Request $request)
    {
        $columns = array( 
            0 =>'name', 
            1 =>'email',
            2=> 'created_at',
            3=> 'id'
        );

        $totalData = User::where('role', '1')->count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $users = User::where('role', '1')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $users =  User::where('role', '1')
                        ->where(function($query) use ($search) {
                            $query->where('name','LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%");
                        })->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = User::where('role', '1')
                        ->where(function($query) use ($search) {
                            $query->where('name','LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%");
                        })->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['created_at'] = date('Y-m-d H:i:s', strtotime($user->created_at));
                $nestedData['actions'] = "&emsp;<button type='button' onclick='removeUser(".$user->id.")' title='Delete' class='btn btn-icon btn-primary btn-danger'><i class='fe fe-trash'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data); 
    }
}
