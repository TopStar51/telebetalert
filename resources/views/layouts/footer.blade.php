<!--footer-->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12   text-center">
                Copyright © 2019 <a href="#">Telebetalert</a>. All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->