<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

		<!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Telebetalert - Admin Panel') }}</title>
		
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Begin template css-->
        <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/favicon.png') }}" />

        <!-- Dashboard css -->
        <link href="{{ asset('assets/css/dashboard.css') }}" rel="stylesheet" />

        <!-- Font family -->
        <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

        <!-- Custom scroll bar css-->
        <link href="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" />

        <!---Font icons css-->
        <link href="{{ asset('assets/plugins/iconfonts/plugin.css') }}" rel="stylesheet" />
        <link  href="{{ asset('assets/fonts/fonts/font-awesome.min.css') }}" rel="stylesheet">

        <!-- End template css-->

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  </head>
	<body class="login-img">
		<div class="page ">

		    <!-- page-single -->
			<div class="page-single">
				<div class="container">
					<div class="row">
						<div class="col mx-auto">
							<div class="text-center mb-6">
								<img src="{{ asset('assets/images/brand/logo.png') }}" class="" alt="">
							</div>
							<div class="row justify-content-center">
								<div class="col-md-8 col-lg-6 col-xl-5 col-sm-7 ">
									<div class="card-group mb-0">
										<div class="card p-4">
											@yield('content')
										</div>
									</div>
								</div><!-- col end -->
							</div><!-- row end -->
						</div>
					</div><!-- row end -->
				</div>
			</div>
			<!-- page-single end -->
		</div>

		<!-- Jquery js-->
		<script src="{{ asset('assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
		
		<!--Bootstrap js-->
		<script src="{{ asset('assets/js/vendors/bootstrap.bundle.min.js') }}"></script>
		
		<!--Jquery Sparkline js-->
		<script src="{{ asset('assets/js/vendors/jquery.sparkline.min.js') }}"></script>
		
		<!-- Chart Circle js-->
		<script src="{{ asset('assets/js/vendors/circle-progress.min.js') }}"></script>
		
		<!-- Star Rating js-->
		<script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js') }}"></script>
		
		<!-- Custom scroll bar js-->
		<script src="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

	</body>
</html>
