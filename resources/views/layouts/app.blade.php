<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Telebetalert - Admin Panel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Begin template css-->
    <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/favicon.png') }}" />

    <!-- Dashboard css -->
	<link href="{{ asset('assets/css/dashboard.css') }}" rel="stylesheet" />

    <!-- Font family -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

    @yield('page_styles')

    <!-- Custom scroll bar css-->
    <link href="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" />

    <!--Horizontal css -->
    <link href="{{ asset('assets/plugins/horizontal-menu/dropdown-effects/fade-down.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/horizontal-menu/horizontal.css') }}" rel="stylesheet" />

    <!---Font icons css-->
    <link href="{{ asset('assets/plugins/iconfonts/plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/fonts/fonts/font-awesome.min.css') }}" rel="stylesheet">

    <!-- End template css-->
    <style>
        .content-area > .container {
            min-height: 80vh;
        }
    </style>
    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="app sidebar-mini rtl">
    <!--Global-Loader-->
    <div id="global-loader"></div>
    
    <div class="page">
        <div class="page-main">
            <!--app-header-->
            <div class="app-header header d-flex">
                <div class="container">
                    <div class="d-flex">
                        <a class="header-brand" href="{{ route('create.alert') }}">
                            <img src="{{ asset('assets/images/brand/logo.png') }}" class="header-brand-img" alt="telebetalert logo">
                        </a><!-- logo-->
                        <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a><!-- sidebar-toggle-->
                        <div class="d-flex order-lg-2 ml-auto horizontal-dropdown">
                            {{-- <div class="dropdown d-none d-md-flex">
                                <a href="#" class="d-flex nav-link pr-0 country-flag1" data-toggle="dropdown">
                                    <span class="avatar country-Flag mr-2 align-self-center"><img src="{{ asset('assets/images/us_flag.jpg') }}" alt="img"></span>
                                    <div>
                                        <span class="text-gray-white mr-3 mt-0">English</span>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <span class="avatar  mr-3 align-self-center"><img src="{{ asset('assets/images/spain_flag.jpg') }}" alt="img"></span>
                                        <div class="d-flex">
                                            <span class="mt-2">spain</span>
                                        </div>
                                    </a>
                                </div>
                            </div> --}}<!-- flag -->
                            <div class="dropdown dropdown-toggle">
                                <a href="#" class="nav-link leading-none" data-toggle="dropdown">
                                    <span class="avatar avatar-md brround"><img src="{{ asset('assets/images/faces/male/33.jpg') }}" alt="Profile-img" class="avatar avatar-md brround"></span>
                                    <span class="mr-3 d-none d-lg-block ">
                                        <span class="text-gray-white"><span class="ml-2">{{ Auth::user()->name }}</span></span>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('profile') }}">
                                        <i class="dropdown-icon mdi mdi-account-outline "></i> Profile
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div><!-- profile -->
                        </div>
                    </div>
                </div>
            </div>
            <!--app-header end-->

            <!--Horizontal-menu-->
            <div class="horizontal-main clearfix">
                @include('layouts.menu')
            </div>
            <!--End Horizontal-menu-->
            <!--content-area-->
			<div class="content-area">
                @yield('content')
                @include('layouts.footer')
            </div>
            <!-- End content-area-->
        </div>
    </div>
    <!-- Back to top -->
    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
    
    <!-- Jquery js-->
	<script src="{{ asset('assets/js/vendors/jquery-3.2.1.min.js') }}"></script>

    <!--Bootstrap js-->
    <script src="{{ asset('assets/js/vendors/bootstrap.bundle.min.js') }}"></script>

    <!--Jquery Sparkline js-->
    <script src="{{ asset('assets/js/vendors/jquery.sparkline.min.js') }}"></script>

    <!-- Star Rating js-->
    <script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js') }}"></script>

    <!-- Custom scroll bar js-->
	<script src="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!--Horizontal js-->
    <script src="{{ asset('assets/plugins/horizontal-menu/horizontal.js') }}"></script>

    @yield('page_scripts')
    
    <!-- Custom js-->
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
