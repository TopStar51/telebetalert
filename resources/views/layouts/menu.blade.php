<div class="horizontal-mainwrapper container clearfix">
    <nav class="horizontalMenu clearfix">
        <ul class="horizontalMenu-list">
            <li aria-haspopup="true"><a href="{{ route('create.alert') }}" class="{{ request()->is('create-alert') ? 'active' : '' }}"><i class="fa fa-info-circle"></i> Create Your Alert</a></li>
            <li aria-haspopup="true"><a href="{{ route('list.alert')}}" class="{{ request()->is('alert-list') ? 'active' : '' }}"><i class="fa fa-table"></i> List Of Alerts</a></li>
            <li aria-haspopup="true"><a href="{{ route('list.league')}}" class="{{ request()->is('league-list') ? 'active' : '' }}"><i class="fa fa-table"></i> League Management</a></li>
            @if (Auth::user()->role == 0)
            <li aria-haspopup="true"><a href="{{ route('list.user')}}" class="{{ request()->is('user-list') ? 'active' : '' }}"><i class="fa fa-users"></i> User Management</a></li>
            @endif
        </ul>
    </nav>
    <!--Menu HTML Code-->
</div>