@extends('layouts.app')

@section('page_styles')
<!-- Data table css -->
<link href="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatable/responsivebootstrap4.min.css') }}" rel="stylesheet" />
<style>
    table.dataTable td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <!-- page-header -->
    <div class="page-header">
        <h4 class="page-title">User Management</h4>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="{{ route('create.alert') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">User List</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->
    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type='button' class='btn btn-pill btn-success' data-toggle="modal" data-target="#user_modal"><i class='fe fe-plus mr-2'></i>Add User</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="user_table" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date Added</th>
                                <th>Actions</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Message Modal -->
<div class="modal fade" id="user_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="username" class="form-control-label">* Username:</label>
                        <input type="text" class="form-control" id="username">
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-control-label">* Email:</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="password" class="form-control-label">* Password:</label>
                        <input type="password" class="form-control" id="password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_save">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<!-- Data tables js-->
<script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var dataTable = $('#user_table').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 25,
            "ajax": {
                "url": "{{ url('getUserList') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{ csrf_token() }}"}
            },
            "columns": [
                { "data": "name" },
                { "data": "email" },
                { "data": "created_at" },
                { "data": "actions"}
            ]
        });
        

        $("#btn_save").click(function(){
            var username = $("#username").val();
            var email = $("#email").val();
            var password = $("#password").val();

            if(username == '' || email == '' || password == '') {
                alert('Please fill in all fields.');
                return;
            }
            $.ajax({
                url: "{{ route('users.add') }}",
                type: 'POST',
                data: {name: username, email: email, password: password},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        })

    })
    
    var removeUser = function(user_id) {
        if(confirm('Are you sure?')) {
            $.ajax({
                url: "{{ route('users.delete') }}",
                type: 'POST',
                data: {user_id: user_id},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        } else {
            return;
        }
    }

</script>
@endsection