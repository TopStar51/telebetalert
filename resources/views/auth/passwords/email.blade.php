@extends('layouts.auth')

@section('content')
<div class="card-body">
    <h3>Forgot password</h3>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="input-group mb-4">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-gradient-primary btn-block">Send</button>
            </div>
        </div>
    </form>
</div>
@endsection
