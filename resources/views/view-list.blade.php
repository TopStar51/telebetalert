@extends('layouts.app')

@section('page_styles')
<!-- Data table css -->
<link href="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatable/responsivebootstrap4.min.css') }}" rel="stylesheet" />
<style>
    table.dataTable td {
        vertical-align: middle !important;
    }
</style>
@endsection
@section('content')
<div class="container">
    <!-- page-header -->
    <div class="page-header">
        <h4 class="page-title">View List of Alerts</h4>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="{{ route('create.alert') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">View List Of Alerts</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->
    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="alert_table" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>
                                <th>Type</th>
                                <th>Match Name</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<!-- Data tables js-->
<script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#alert_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ url('getAlertList') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{ csrf_token() }}"}
            },
            "columns": [
                { "data": "type" },
                { "data": "match_name" },
                { "data": "start_time" },
                { "data": "end_time" },
                { "data": "status" },
                { "data": "actions"}
            ]
        });
    })

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    var removeAlert = function(alertId) {
        if(confirm('Are you sure?')) {
            $.ajax({
                url: "{{ route('alerts.delete') }}",
                type: 'POST',
                data: {alertId: alertId},
                success: function(data) {
                    if(data.status) {
                        window.location.reload();
                    }
                }
            })
        } else {
            return;
        }
    }
</script>
@endsection