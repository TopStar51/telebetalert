@extends('layouts.app')

@section('page_styles')
<!-- Data table css -->
<link href="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatable/responsivebootstrap4.min.css') }}" rel="stylesheet" />
<style>
    table.dataTable td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <!-- page-header -->
    <div class="page-header">
        <h4 class="page-title">League Management</h4>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="{{ route('create.alert') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">View List of Leagues</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->
    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <button type='button' class='btn btn-pill btn-success' data-toggle="modal" data-target="#league_modal"><i class='fe fe-plus mr-2'></i>Create League</button>
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align: right">
                            <div class="form-group">
                                <button onclick="playAll()" class='btn btn-app btn-primary mr-2 mt-1 mb-1'><i class='fa fa-play'></i>&nbsp;Play All&nbsp;</button>
                                <button onclick="pauseAll()" class='btn btn-app btn-secondary mr-2 mt-1 mb-1'><i class='fa fa-pause'></i>&nbsp;Pause All&nbsp;</button>
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align: right">
                            <div class="form-group">
                                <select class="form-control" id="filter_league_status">
                                    <option value="">--League Status--</option>
                                    <option value="0">Not Published</option>
                                    <option value="1">Published</option>
                                    <option value="2">Pause</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="league_table" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>
                                <th>League Name</th>
                                <th>Market Group</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Message Modal -->
<div class="modal fade" id="league_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New league</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="league-name" class="form-control-label">* League Name:</label>
                        <input type="text" class="form-control" id="league_name">
                    </div>
                    <div class="form-group">
                        <label for="group_id" class="form-control-label">* Market Group:</label>
                        <select class="form-control" id="group_id">
                            @foreach ($groups as $group)
                                <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_save">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<!-- Data tables js-->
<script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        fill_datatable();

        function fill_datatable(filter_league_status = '') {
            var dataTable = $('#league_table').DataTable({
                "processing": true,
                "serverSide": true,
                "pageLength": 25,
                "ajax": {
                    "url": "{{ url('getLeagueList') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{ csrf_token() }}", league_status: filter_league_status}
                },
                "columns": [
                    { "data": "name" },
                    { "data": "group_name" },
                    { "data": "status" },
                    { "data": "actions"}
                ]
            });
        }

        $("#btn_save").click(function(){
            var leagueName = $("#league_name").val();
            var groupId = $("#group_id").val();

            if(leagueName == '' || leagueName == null) {
                alert('Please input a league name');
                return;
            }
            $.ajax({
                url: "{{ route('leagues.add') }}",
                type: 'POST',
                data: {league_name: leagueName, group_id: groupId},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        })

        $("#filter_league_status").change(function(){
            var filter_league_status = $(this).val();
            $("#league_table").DataTable().destroy();
            fill_datatable(filter_league_status);
        })
    })

    var removeLeague = function(league_id) {
        if(confirm('Are you sure?')) {
            $.ajax({
                url: "{{ route('leagues.delete') }}",
                type: 'POST',
                data: {league_id: league_id},
                success: function(data) {
                    if(data.status) {
                        window.location.reload();
                    }
                }
            })
        } else {
            return;
        }
    }

    var changeStatus = function(league_id, status) {
        if(confirm('Are you sure?')) {
            $.ajax({
                url: "{{ route('leagues.changeStatus') }}",
                type: 'POST',
                data: {league_id: league_id, status: status},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        } else {
            return;
        }
    }

    var playAll = function() {
        if(confirm("Are you sure?")) {
            $.ajax({
                url: "{{ route('leagues.playall') }}",
                type: 'POST',
                data: {},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        }
    }

    var pauseAll = function() {
        if(confirm("Are you sure?")) {
            $.ajax({
                url: "{{ route('leagues.pauseall') }}",
                type: 'POST',
                data: {},
                success: function(resp) {
                    if(resp.status) {
                        window.location.reload();
                    }
                }
            })
        }
    }

</script>
@endsection
