@extends('layouts.app')

@section('content')
<div class="container">
    <!-- page-header -->
    <div class="page-header">
        <h4 class="page-title">Edit Profile</h4>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="{{ route('create.alert') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
        </ol><!-- breadcrumb end -->
    </div>
    <!-- page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <form action="{{ route('update.profile') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="email address" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="form-group">
                            <label>Current Password</label>
                            <input type="password" class="form-control" name="current_password">
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group mb-0">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success mt-1">Save</button>
                        {{-- <button type="button" class="btn btn-danger mt-1">Cancel</button> --}}
                    </div>
            </div>
        </div>
    </div>
    <!-- row end -->
</div>
@endsection
