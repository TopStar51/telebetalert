@extends('layouts.app')

@section('page_styles')
<!-- Time picker css-->
<link href="{{ asset('assets/plugins/time-picker/jquery.timepicker.css') }}" rel="stylesheet" />
<!-- Date Picker css-->
<link href="{{ asset('assets/plugins/date-picker/spectrum.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    <!-- page-header -->
    <div class="page-header">
        <h4 class="page-title">Create Your Alert</h4>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="{{ route('create.alert') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Your Alert</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->
    <!-- row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form action="{{ route('save.alert') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Bet Type</label>
                                    <select class="form-control custom-select" name="type">
                                        <option value="">--Select--</option>
                                        <option value="Double Chance">Double Chance</option>
                                        <option value="Asian Handicap">Asian Handicap</option>
                                        <option value="Half Time/Full Time">Half Time/Full Time</option>
                                        <option value="Textual Search">Textual Search</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Match Name Or Team Name</label>
                                    <input type="text" class="form-control" placeholder="Germany Women v Spain Women or Germany Women" name="match_name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">URL</label>
                                    <input type="text" class="form-control" placeholder="Paste a specific match url or league url" name="url">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div><input class="form-control start-date" placeholder="MM/DD/YYYY" type="text" name="start_date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Time</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-clock-o tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div><!-- input-group-prepend -->
                                        <input class="form-control start-time" type="text" name="start_time">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div><input class="form-control end-date" placeholder="MM/DD/YYYY" type="text" name="end_date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Time</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-clock-o tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div><!-- input-group-prepend -->
                                        <input class="form-control end-time" type="text" name="end_time">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="reset" class="btn btn-danger btn-reset">Reset</button>
                        <button type="submit" class="btn btn-success btn-save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<!-- Timepicker js -->
<script src="{{ asset('assets/plugins/time-picker/jquery.timepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/time-picker/toggles.min.js') }}"></script>

<!-- Datepicker js -->
<script src="{{ asset('assets/plugins/date-picker/spectrum.js') }}"></script>
<script src="{{ asset('assets/plugins/date-picker/jquery-ui.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.start-date').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true
        });
        $('.start-time').timepicker();
        $('.end-date').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true
        });
        $('.end-time').timepicker();
    })
</script>
@endsection
